package partition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ListUtils {

	

/**
 * 
 * @param <T> la classe des objets du tableau
 * @param list des objet a diviser 
 * @param taille de type int doit etre sup�rieure ou egale 0 
 * @return une liste de tableau des objet  diviser par la taille 
 * @throws ListUtilsException dans le cas si la taille et inf�rieure � 0
 */
	public static <T> List<T[]> partition (T [] list , int taille ) throws ListUtilsException {
		
		

		List<T[]>  result = new  ArrayList<T[]>();
		
		 if(list==null || list.length==0  ){
			 // si la list est null ou vide  on retourne la liste vide 
			 return  result;
		 }else if(taille ==0) {
			 // si la taille = 0 on retourne tout la liste
			 result.add(list);
			 return   result;
		 }
		 if(taille <0) {
			 // si la taille inf�rieure � 0  on lance une exception 
			 throw new ListUtilsException("La taille est inf�rieure � 0");
		 }
		 
		 
	
		for (int i = 0; i < list.length ; i=i+taille) {
			int begin = i;
			int end = (i+taille) < list.length ? i+taille: list.length;
			
		
			result.add( (T[]) Arrays.copyOfRange(list, begin,end)
					);
			
			
		}
		return   result;
	}
	
}
