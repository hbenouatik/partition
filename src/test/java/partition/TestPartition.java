package partition;



import static org.junit.Assert.assertArrayEquals;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestPartition {
	
	
	@Test
   public void testPartition() throws ListUtilsException {
	   
		
		// test List avec Type Integer et 
	
	   Integer[] listInteger = new Integer[] {1,2,3,4,5};
	   
		List<Integer[]> listPartition2 = new  ArrayList<Integer[]>() ;
		listPartition2.add(new Integer[] {1,2});
		listPartition2.add(new Integer[] {3,4});
		listPartition2.add(new Integer[] {5});
		
		 assertArrayEquals(  ListUtils.partition(listInteger, 2).toArray(),  listPartition2.toArray());
			
		
		
		String[] listString = new String[] {"1","2","3","4","5"};
		List<String[]> listPartition3 = new  ArrayList<String[]>() ;
		listPartition3.add(new String[] {"1","2","3"});
		listPartition3.add(new String[] {"4","5"});
		
		
		
	     assertArrayEquals(  ListUtils.partition(listString, 3).toArray(),  listPartition3.toArray());
	     
	     
	     List<Integer[]> listPartition0 = new  ArrayList<Integer[]>() ;
			
	     listPartition0.add(new Integer[] {1,2,3,4,5});
	     assertArrayEquals(  ListUtils.partition(listInteger, 0).toArray(),  listPartition0.toArray());
			
			
	   
   	}
}
